import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

Color warna1 = Color.fromRGBO(18, 37, 63, 0.5);

TextStyle fontApp(
    {double size = 12, FontWeight? weight, Color? colors}) {
  return GoogleFonts.rubik(
      fontSize: size,
      fontWeight: weight == null ? FontWeight.normal : weight,
      color: colors == null ? Colors.black : colors);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
      theme: ThemeData(
          primaryColor: Color.fromRGBO(18, 37, 63, 1.0),
          accentColor: Color.fromRGBO(18, 37, 63, 1.0),
          appBarTheme: AppBarTheme(color: Color.fromRGBO(18, 37, 63, 1.0))),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var yearNow = new DateTime.now().year;
  String _nameAppBar = "Dashboard";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(_nameAppBar, style: fontApp(size: 20, weight: FontWeight.bold, colors: Colors.white),),),
      drawer: Drawer(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UserAccountsDrawerHeader(
                currentAccountPicture:  CircleAvatar(
                  backgroundImage: NetworkImage('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAADCCAMAAAB6zFdcAAABgFBMVEX6xsfK6vWQoav///9JhU/64GhpwHDcsKviq1JDRBjYo1BXTx+eezror1X6u1zQnUzLynnIsVLexVxcUiGikUL/5WrAlEb/ysxJf0pVgUarmEWZrbfq0mHH5vGrws251eCBe0Q/Sx9uZCrc8fju+PxfWyX839/+8PBGYTOyy9bRuVY8NwCTpbHxv76DdjTQ8f6UhDuNd12409e3pUr70dFOShylu7aJmJxRk1fkzV/mt7M4OgDIoZdKRR6ujn9yaiZWTAB2ZkmOn5VGQQB2gXBdX0FjaVZmWxRoWzp9iodis2mwkIAwMwDLo5qdsa1dZVNSUis6MgBnbE9JTBk4KgBKc0BjqmFLaDaDb1WIh1mzs4HduKvLyZh2dEfc2qejoXJhYTL9+sZ/f1IgKQBye2krJABlWjXU0siFlIdOUDHd4+Hw6qmej3waEgDv5JKflVFvdWyy1a6QsYRme0zj89+b053N6ctPWyZevGeZ05t8x4F8oW91r3GHhEeohD2EazDZjysPAAAMBElEQVR4nO2djVvaSBrAxeWjEds9FB2kUleUVg0BQUcqEbo4hgOsFmX5cLcf2x5LZW/vzn6yt3dn//WdJCCETCKKPSYmv6ePtgo+zM933vedmTSMjVlYWFhYWFhYWFhYWFhYWFhYWFgYBg6M+hWMGpAuJkf9GkYNSPM1swcCSJf4A7NLiJ38+JPJHXCNk+f8jqklABf/6vnJvrkd1F6cnLx8YebSAFzw1cnzk59NHQixn09evn7zJm1iBxx89ebl65M3sVG/kBHievoSx8GrNxlu1K9kZICd0vNXr80cBwDgyvj8x9dSPgCmzAjcwYHrgMWV8ccX7rQL/2PUL+j/D9hHUEHDhDkhyXvXunhCZswJIIaCXULQZcKMALg0H+ngrh1sms9BMpPLMdDdhmnmGuZrmLlc0O/tYf2p+VbQIB1am+ph3Yx1gXvq9fQQSo/6BY0AEGMYBjIS+FPDlHsInCuG1mVC0IxtogjgMsF5B2aDMV8+7IDXTJ4Zh2P+iRl7xAtq69jBFDJjj3hBTHZgynzYwXIwxmX8lgPJwXbIzCeOHQfmLY2WAxHLgbiA3rAcpNbEPtFygHtlU583Sg4clgPLgeXg6ZTD5PkAcH+T9lAipnQgnjMnk8mDp6KDmfUa/js3ZqKzZ3H8B/uxWs6NeEZ2wCN3IxNL7yRNcQYPQPIg3UA8z6KiUK+340CoHwslluV5WNtxjd1qDfi3vBNrIBah49PDeDweCPwiO8jbAvH41m4dIpaFtTR3a6MBjLliJZ4t1Xe3bNFo1GazRbfQhmdGdCD+S/xi/HBPwOGQ27mVV/TjHJCBLDzejQekAUtsfS858NYDna9EbfGtvSLic+lbd/QGxnYaPFvc3Yp2BXQd+LsOJA+BwzriYTp5m2JBjAGEhN0FWx9kB6KFeB2yjfTtmREgGcMxcBiI9ivQdCDlij2Wb+zcjuwIuH2IirsEA7oOxO/WEardhi1nkMwgdi9OMmCL7j7x4gXDjD9LdGCzHeIsegsuT9mBrLBFHuGFA48Q13hAYA+hmNGzQhod1clBMIgDMRQQyhm6QHAZHu5qGRjMQTR+bOj5kGzwSHt0gzmw2cT5YKBN10dK3r59u7y8vKTt4DQygAOsCvGGuULh4TdEtAe3t74+Lzpo6jqQkoJRLtnafERSsKzjwCs6cGwzlziIHkLjRAIAm/hPW8WvGlX/6g5wv2ScSJDZ7ERAL4S0cAUH0cNiyUCJUSsvqCW084GugyXiz3o06iGq2XyogJwa1XkhmvVf6oD8s74Z9YhVaAz68jh4tiFej+RwvFCtqo0WB9oKLssHbQczf9fuJW1LMuKP+/X4t02ZUQ9ZhaYB7YH1OXBfmhNty5KEX9vdlxQLD0c98C6b11XQdfAPogNFWSHOCGriQWsqaPfIF3QcfEdyoDnDutATCQM6CCz090wBwSPnxH8SFCwN4ICe1EjskNVzYXVc7cDvleLgX6Q4IIY/rQ60JPQFwmpY0wE6JBWGJSUEJ/TMBZwSHvXk655IUFRFHQffEx1cGhg0KbhAIz0u3YiDpe7YRTYp/Q+RupNC7SDedbCwurqq1SwukX4mPXWxD91NFIKDnzfaDnbvj4+PP9BYa5OTI5UTQeSn33aX1Gg78PslB0+yYexg/D5ZgsEc5FjtmU10sCE5iOg6UDaMy8tv3+KWmVoFSb6ovXd0fQd9nP5C8ZYSSLN72gn+xhzEoZveixO4DKt1rEZ0sBXauI4DW5beW+0BFxS0RxHFDvqCBDuQc+L6s6s4iO6yMWodpPlTrde9EL4fHr9/QceB3yM58DavFAdxNkfrZAA5tKWVDhbGewlfOFi7joOAAGmdDICHmoO4UQe2U5bSU1hwwAuaVWFYB4FebIeI0lMnXBlPr+QgevjXjUEdPOh9diBepPSuGSB2pF0ZNRx45Jy4cUUHNoGn0wGXKWlvD2vFwba4lTbjSV3NQbRO6T1Ykw2dywi0HMxfzwGtSdGFtC4vu3kHh3R2ScDF7mkO4YYd4C6Jyrtzgx2dskCujaeR6zpYOMpQ6WCf1bkErc+BXOZPI37ZwRpMXM1BAFLZLeP2YGAH7ZXDWcR7XQclOh3EWM3VgsqBTLYzF3octDvByxwIbhpvqCQ60FSg5UBqD3odRKUAeaCS2e/g2BgOAgsiAR0HzyKevjgIyGO9/6CXBeM6aA9Hx4Eg7axjB1MdB8SHGdBBZ3XXdiBPbl0Hju1Qvu0gbHgH0jReCIelSd15xQ9UDsLtj0M4KDaodNCpjat9Q+hzEB7Pn2UFQcie5ZvXd0Bpf7A/mIO8AEutlZW5cquUur4DSvuDTq+s5yCcF9BK2W5/XHA6nRVmQ8dB928EB7T2yi5eXjPpOcjDls+OFTglGE/XQVjhIJFI5POJhCxC7SC6ReeaacwFjwP6DsJnqGrvKug6mA+eSQ5sAelhibMmOiqhH9Czs0SY6IDStfPFHspqO5QTIvhz10FeqUDlYFwqJoksbM1V8Lffvf9QSp2pHeCH1Xk691C4DLpwgH+V2WYKppr4N/kgihEdJJotUYHdqelAemaxJGUL5x+fP35+94HNqh2MhwVKT9twgyAdvK/icdRRqVUt+8rllRKqJ9ovPFuSFBSIDtpBE86nqt0HfPr88X1JUDtIFBGNZUEqjlJhWA0nmqjqs8v4yqiIJ3U4nMhD5UxwOn+YcnQcFFMQpgQc+KlW9/vO353vPr5D4lm20kGe2ncqcPFF0cFCO/l38LVgHpdEhEo+ZRg4f9huO3BEqpVC4fFcC8JeBaKFf398f4T7T4WDcJ7WM5bOWdsWbNmVrOChlX0+X18YdB3MROakLxTmYMXZx6fPH3C9UcZBFtGZEsU33RKvtQwc9yuw26vduKjoOSDx++d3eCGidACpfbcGsM+fBmy7yKdy0EPhqg6cnz626koHCZQb9Vg1cbmL4+PFqp6CXgeFwRz88fE/7P1eBeE6orNDkqihfB7qhkFvOqi452fa6Dlwfvovm1CspQSe1qkgTYbsWUk56PKKgupclzLsvAGFP9ia08ZZgcJZj4U8nQvnNhyCzRWFAh/8bljcc06xanY7ySyi+f5qoMZARTrwLYb+Miz/O6pIVTMflqG4KoiAA9jnALqn3UMynSqLWaEq5PPZZjObryMq9w4u4DJKB/bFoGdY/EdST1FJpRDOJi0EKV0vdQA7vLJDKsONqSEJVuXyUJVrSqFUozgjinCNkrI2Lj7xDsd6X/s8R/1NMcA+Uk6G8uLiYophYCg0TUQvEzCpI6bVKiu7BTZHuQJMrq9J8uHlcwu6gyQikYhOAEzD1lyhUFAqmGNpDwPpKkVlhyCLWIFu8nD9GniDR9XHThUVWBv1CAchBstqCeIuwhOt8RJYd7cIBpzOFm+EmyWBJCwRHODMwIS8fmIsqIMj0ruj1sNjluYWsQtOi4TZIIbC4rRnsHKIFdhJDirQMO8AnGFIswFLYIKOGcelzHhSYm0hSGhRvGDsg3NrLKB9zPoACtaO5PKqUlA2yEwQATuoRZZQPvJcGgjzwXar2b+1+JjNjHpkVwDss+pNRYkV9/xlYeBl2v4eFwoVTGdOVCC9O2hEaoi8pea7OGvWBPY/Uz52qpSoXjIT4GosWcJKUD8QZjYY9TTCrUKhBelvEJUArqH6fcqBcDSl68ARIRXWggEViK1SgxwJi37dybAdItbVFtX7Z1pgCcRIWInoOZhZI0wFuw+i/VGP51qAZIYUCdWQrgMPo35KuWTUu4lyXDIHW75+qtPb8zp4F+2qZ0C4zxnxPsvAlcvlmgyzqIIJ6QHVz2CYVC7XMGA+4IrB2Zvk3Fi3TxQBaTRx70aZLBph56AHsM+f35kYijv37t2duHuv+1PcRlotYDgIyVuoV2D2zhc4+W3P7itjqJTAZaYnh+Z89nzy/LznC18oP1xRAGL87LdfgS/GeVN4cMBP3r0RcCpsf5CYcNN5hS4B3CQPe87a5nxidhpOTnzpnL5CSi9PVQFqaPhk0E0J8odOSkgZYg8BpN2zw1VFPb7AUY9vAICLmbx756sxa4RbjoNYCTJfEUjlf+bqg3N9ZUY9QAsLCwsLCwsLCwsLCwsLCwsLCwuz8if2TBA1pVvEmAAAAABJRU5ErkJggg=='),
                  backgroundColor: Colors.white,
                ),
                accountEmail: Text('iskandar@gmail.com', style: fontApp(colors: Colors.white),),
                accountName: Text('Nandar Iskandar', style: fontApp(colors: Colors.white),),
              ),
              ExpansionTile(
                leading: Icon(Icons.task),
                title: Text('Master data', style: fontApp(),),
                initiallyExpanded: false,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Column(
                      children: [
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Master Sub 1", style: fontApp(),),
                            // selected: ,
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Master Sub 1';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Sub 2", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Master Sub 2';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Sub 3", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Master Sub 3';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Sub 4", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Master Sub 4';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Sub 5", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Master Sub 5';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Sub 6", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Master Sub 6';
                                Navigator.pop(context, true);
                              });
                            }),
                      ],
                    ),
                  ),
                ],
              ),
              ExpansionTile(
                leading: Icon(Icons.task),
                title: Text('Transaksi', style: fontApp(),),
                initiallyExpanded: false,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Column(
                      children: [
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Transaksi Masuk", style: fontApp(),),
                            // selected: ,
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Transaksi Masuk';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Transaksi Keluar", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Transaksi Keluar';
                                Navigator.pop(context, true);
                              });
                            }),
                      ],
                    ),
                  ),
                ],
              ),
              ExpansionTile(
                leading: Icon(Icons.task),
                title: Text('Component', style: fontApp(),),
                initiallyExpanded: false,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Column(
                      children: [
                        ExpansionTile(
                          leading: Icon(Icons.task),
                          title: Text('Widget', style: fontApp(),),
                          initiallyExpanded: false,
                          children: [
                            new ListTile(
                                leading: new Icon(Icons.add_shopping_cart_rounded),
                                title: new Text("Card", style: fontApp(),),
                                // selected: ,
                                hoverColor: warna1,
                                onTap: () async {
                                  setState(() {
                                    _nameAppBar = 'Card';
                                    Navigator.pop(context, true);
                                  });
                                }),
                            new ListTile(
                                leading: new Icon(Icons.add_shopping_cart_rounded),
                                title: new Text("Container", style: fontApp(),),
                                // selected: ,
                                hoverColor: warna1,
                                onTap: () async {
                                  setState(() {
                                    _nameAppBar = 'Container';
                                    Navigator.pop(context, true);
                                  });
                                }),
                            new ListTile(
                                leading: new Icon(Icons.add_shopping_cart_rounded),
                                title: new Text("Elevated", style: fontApp(),),
                                // selected: ,
                                hoverColor: warna1,
                                onTap: () async {
                                  setState(() {
                                    _nameAppBar = 'Elevated';
                                    Navigator.pop(context, true);
                                  });
                                }),
                          ],
                        ),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Component 1", style: fontApp(),),
                            // selected: ,
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Component 1';
                                Navigator.pop(context, true);
                              });
                            }),
                        new ListTile(
                            leading: new Icon(Icons.add_shopping_cart_rounded),
                            title: new Text("Component 2", style: fontApp(),),
                            // selected: Sub1(),
                            hoverColor: warna1,
                            onTap: () async {
                              setState(() {
                                _nameAppBar = 'Component 2';
                                Navigator.pop(context, true);
                              });
                            }),
                      ],
                    ),
                  ),
                ],
              ),
              new ListTile(
                  leading: new Icon(Icons.add_shopping_cart_rounded),
                  title: new Text("Log Out", style: fontApp(),),
                  // selected: Sub1(),
                  hoverColor: warna1,
                  onTap: () async {
                    setState(() {
                      _nameAppBar = 'Master Sub 2';
                      Navigator.pop(context, true);
                    });
                  }),
            ],
          ),
        ),
      ),
      // body: Center(child: Text("Body \n$_nameAppBar", textAlign: TextAlign.center,)),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth > 600) {
            return _buildWideContainers();
          } else {
            return _buildNormalContainer();
          }
        },
      ),
      bottomNavigationBar: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if(constraints.maxWidth > 600) {
            return SizedBox.shrink();
          } else {
            return  new Container(
              height: 50.0,
              color: Colors.grey[200],
              child: Center(
                child: Text("My Drawer\n"
                    "\u00a9 $yearNow ECI, ALL RIGHT RESERVED.",
                  textAlign: TextAlign.center, softWrap: true, style: fontApp(),
                ),
              ),
            );
          }
        },
      )
    );
  }

  Widget _buildNormalContainer() {
    return Center(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFRUYGBgaHBocGhwYGhwaGBwaHBocGhgcHBocIS4lHB4rHxwcJjgmKy8xNjU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHjQrISs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAADBAIFAAEGB//EADwQAAIBAwMCBAQEBQMDBAMAAAECEQADIQQSMUFRBSJhcRMygZEGobHwFEJSwdEjYuFykvGCorKzBzND/8QAGQEBAQEBAQEAAAAAAAAAAAAAAQACAwQF/8QAIxEBAQACAgMAAAcAAAAAAAAAAAECESExAxJBBBQiQmGR4f/aAAwDAQACEQMRAD8A6hqgagzVFrlaTVyq26YNPF6XuqDUiz3QBSFy8ScUzdbpQkt1IXToaaVajaWjAVJtVoiitA1B7gFSFxWjSyXDNMA4qQLkmtotS21ICoNTS2ptMRim9lB1N7aMCgqpdOV5rGf0rWo1cc1EOsTQgtL5nwKs71rcIIqr017a8gU3/GsTxipD2NIRk0YiOBS66tsYqwsWWbkVoFPhk0e3pDVimmAoxtxUVZqSltC7sFURJPriuX1/4pUMRZVSo/neTPsnbPM/Slv/AMg65zdFokbFAICwTJ53Z+wjtXI/FmBmB+4NFqei+F/i603lujY3cZX/AIroyVZSymRnI4xzFeLbo4/XpV74P+IXs+QeZSQCCSAJ5A7Zk1bOnoF7WAIRVGNPv4NNko9rep7/AEIMEfeqfTbwfKTQFnd0u2IOaatIwE0HT2W3jecEVZXLqgQKUXtEzmrfTPCzVdtyPWrA2vKBSIj8c96ys/hqyoh3LtRFyg3XoIeakPcu0NnrGShgVAIpmaMi0REoypQUESigVIJQy1KbY4pVlJNGa5NRVqgyKkjZqNbg1IcisFBF7oaZQYoKF14FJJd3TNO32ilGs9ZoRA6ZGeWNC1yKvy1jgbjmgXEPWpA6RvNTj34xFIadfMcxVpp0XkmpH9EgkAir+2lUC3FlYNXSalYGaUPFBuvmKMHpTWXVUgmlOQ/HemHwyYG74qsp/qVrexgD6Mg+69689NyDjp9f/Nev+P8AhS6m2Qp2uAdpBMHg7WjlZAPoQDXmmn8BdnZLjbGUmVIlzHJEGCPYmjSU5c/vpXVeD/hZHUNcvbWIBKKFO0HjcZP6UDTaWzYeX23COVccZwy9D3+kdcks3UVxeDqDuJyWVyPQqNpHvjvXPO2cPT4MMcua7nQeDpbti2vmQDqcmczI/tRR4WijyCPczVFovGBgFXWeIGPqQdo7zIq1GoVus/YGuXvY9X5fG9a/oLU2m6nArdiyOa38YLJwy9dy/wB6S0+s52gxJia7YZzLh5PP4L4+fiyW95qaW8WNV1pxM1aW2GIro85lXrKjvFZUldqEHFDtWac1VsTJrdhPSpMKCKWeBTV87aWLAmKENYGKLFQBAGK0L1SbZ6E+a25raLSgyKxVijm3WisVIFea21yoPNRZqgmqTzTSCBULYxRG4oJTU3opDUarHNG1iE1X39MYrKKNczNaOs6UC6pHNKC5BpQy3Duq00V9WG1uaolfNMacZmjaXZ07Dg4psakIoL7o7hXaPfaDH1pTQX2cxGBVR4p+IHt3GVYZi21JjyiY/lOcgwOardNYyXt3Gk1QdAyNIIkHuKzVKGia47wXxV0uN8fUB9wURgBGzzgDtgd66fUakLE9a0KB474v/DWwVgu7BEU8SeWI6gc+uBXBeIaxmcsSd24tu6zM7p/eKtvGrq3tSGD7hbUhk2/IQSAd0+YkwfoRVFqlGQD7e37mlkHW6zfBIz1+vP5wadtWA9pXUDAh16E5yex9f2tExzV14BrEQsCGJ3EwMysAER/NxMc9elc/Juzb0/hrjMtZfUra24gGG6KzG3n0bhh6GPem9OxG4izs/qdnCADspYYE9BNGv3bDoVRW3Pt2h4FtDMHdPmGOuBxntV2/ByylgPlZlgeXIMHmYrlrjdez2sy9cVlfZ74KW28oGVRixP8AudoAOeBIqw/D2ldA6OGIkbZBHTzRPTjjvQb1kWSiZMgQQWKeUBCGRCM9Zg0x4Zqm+KoIiZgA7lPMZ7yJqxy1loeXxzLx3K9rdbRBzTK7oxW77TyIqQws16XzEPiHtWVLeKypLK/bmopeA5opqq1gMx0NCG115SIBzSiCorpYM0wLdSbL4qHWpBBU1WlNotHRKioqe+KkMLZoOogCsuakxSj3qk070vurHehpJMAEk4AHJJ4FCH+OaIbxirnxX8PG1YVhlwN1wc4Pb/p/PJrn5gUJG7eIqvu6g0yzTzSd0yakr9beJ6UopEZqxvL1oGwEZo0idsS0VZWdNSaWZbFWlm2wPemRHfD9SEwRXN+Nad21bvaVVGzcxBgRBknsxgjHP1NdLdQYMVW6HTi4Lzzi4xQH/ao2j891G2pONuNcPZco0bkYgwZE9SD1/wCacteOXF4Y/r+vFdY2gR9wdQwZt0HodoWQRkGBVP4n+EwPNZeB/S/9mH9x9aWVQ/iLEkgAFiS0YDE9xxPOfWk7931pcuRUlQtGMnApSAMmi2Z3yDBGZ4/PpQgIOeRU7cQc8kD9ZqpnbpXv7g6sq3GVF8p8pAZQS6Rywn98F7TqvwgMruHzwVkkRtZf6vf79KpPENUEuPtUbwxBeAzYG2FBwoj3pnQK72SNwEGSGbzkgyCD3AP6Vwyx4fR8fklzs7p9rzhFZ2UDcwwCwHmYeZpwDsJ+1aZAbik/K3zQI6rPAg4PJmlk1bQ1p/K27duA5k5n1nb07jqaIXIUTAPG4QBEHGBjj95rGtV18duWNmTttVaYALgggQR0PYA9Ig84z0il9jDBotrUF0tuOCoP+amz969T5FD+HWUXfWqUsC1Ae3NFE1MLQSeBWiRRNRFKo9ISC5phVoVumFFSbUVp1qZobNUi9/iknerFqRuWqEV311/4J8K3H+IYeVSQk9W6t7Dj3ntXO+FeEtfurbUwDlj/AEqPmPv0HqRXqELZRUQABQAo7AVJDXiT3gZ+vE151434cbNyAPI+U9O6/T9CK7+1fVTLMAGwSxAEngSepNL+N+FC7bZehyp/pbofb+xoTzZyAKTKg0fVKVZkbDKYI9RQbdSZ/ChqT1emjAq0RJqT6URQnOpuDVcaS6RzSt9RvFFc59KUPrru227/ANIJ+vT8614ZZZLSq44UT78n86HqCH+Faj53Wf8ApTztPoYA+tXHiAxAonNavEkI23XJqF24AjmeFY+0A0fT2BxFC8VsbbbmP5H/APia0y80VATH68x3PatXWkz06DpFHaFScS5MD0Bifv8ApULOlLZJCju2PsOakHdJJ3HJOSfU80fw23udByC6D/3Ca09nEbg2QMdDmP0NNeFn/UT0J/IGi9NYTeUn8ta9gbtzj5259yKa8GPzBfmHmTkieCInjiqp7zSTnJJ69TNGsOwMyQeRntmP32ouO8dN4eT0z9l9qnVwrukpADR8yOB074g/XvNDv3QFUK8gEGSOkz9+lVg1xk7vlf5o69jHQip2WHQg+5rjcbO3ux8+OU4/16J4Us2Lcf0A/U5P50bZmkvw1qg1hOwlftzVuxFeidPnZzWVA21qiRWUsnUcVLdSFnUAyZxwO1MG8qiSaydF9UDSioRT73AVkGq3U6qAAIlsCnoGbBM0/bFVdjVQm4wCDBnFWaGc1SnSbigOKZIoDikF3agM81YP4VeP/wDG5/2N/ilrvhd5QWNpwoEklGAAHJJjiip234W8PW1Z3Aqzv5mKkEY4UEdv1JoPi3iiWlLuZJ4UfMx7AdvWuN0dvU2XUoLiF/l8phuvykQ3ToaLr/D9S5Lvauux/wBjfYADAoSt1vib6hy1wwo+RB8qj+59f0GKsPB/xZc0/lb/AFLXG1j5lH+1j+hx7Us2he2k3LLoOJZCBJ4yRQn8DvnI092Onkb/ABUll+K9Nb1Fv+L0zbigAuLw4XoWXpHfqOuK5TTuTV0vhWpQF1tXbZUGW2MPLHmmRBWOQcULTeB6gru+BcIIkQjQQciIHFFQSsamSxpm34PqRzp7v/Y3+KNpivWpKU2vOJqw1OlETWtUi7wRTrMpEnina0p/Dbe7Us0YtoF/9TncfqAo/wC6rh/M0GlPw3b/ANNnIzcdnPsTtT/2qKcvqAZmjHpq9pfDC8VXeMXh8NyxAUI0+xUiianVTAzmuZ/Gep2IiAxvJJHcLH5SR9q0HOaW2XYNtVzIVUZu0ASJ+XzD7HpNOqwBIdyIOIQhfUoVYMBP0MTVVZuBGV58x3SuZXECffn6UxqfFS4AKAkcNwRXPKW16PFlhjjz2Z8SvhlMkuRthsSVg4MjkE8jkRVXbusDuUwf3xQnuE1H61qTU0455by3DWmwZOffNN29KzsBbX5jAk4BHJB7QJPtSKX9oxzwJ6evvW3uOpB3mSo4PQ5j9KWDNx0yCxDAkfLiRzMGgM4iBsPrBB+9Lg1Y+HWXUpf2rsDnaXBKF1EjdHCgkZ6H61J6H4Rp1TT2gvGxT2mRJMepJNWdsUn4FdFywhAI2ypBKkhlO1sr5TkdKsQtaTUVlb21lSc1o9WoSZ8kkmeJMxNInxUhypIjceOR2APtVdeugIUmCWEj64+hpNggmH3MDgetcZa6WOnfxkgfLPel7Xig2+fHJE95xXPpqGJ7RM+1WCaPchYmcTA6Ue1nYdI6b03cBhP1jmm/CtcmxVLiVxz2rh/4q5hdzBRjrTOg00uqtO1uo59xNa9tLT0a3cBEgzR9GP8AVtn/AHp/8hXFeHF7M+ckFoIORzz6V0dnVFhxxTM5WLXbeLq2+VXVEbR/+l0VeT0Ygz/xVP4nvFq4SmtA2tJe4hQCM7gGkr3iuWGsd2w7df5jx96FbuuQ+53KnEFiR6jmqZRPQ7jRd0Hqtz/6RVVrrGrOpcxqTZ3GBauBTEY2ywAE1x29iwliVGVJY49u1bOrct87kHjzNH61XKF2Piuk22Euu+pB+LbGy/cDAf6gEkDHEnmrTxxX3jaurYbebDoqzJ5DEHd69orzXUIzcuzDpLEgfetC+8AK7/8Ae0frWblA7HxBLnwn8mvHkbL3UKfKfnAaSvf0repZNPY07ve1Z3qnlS4AAdgaIMQK4mxfcOwZ3MgcuxEA9pol/eWCsWPUAkkD2HSr250noX4b1lu5ve1d1DuoI+FeuA8wQQOMkRM4zXFGd77lKsWYlTyCSZB9qR0zFZKkj1BjHaaYtXJMzJ6yZ/OtS7MavWzNQ8TcrZIEbnhFkxJc7f7mmWf0pW4xe9bTMJucieYG1ZHu0j2NVM7XOnthEVV4UAD2AgUvrCenWiFsc1T66+S6RkCSc44oyy0E9dfTYqsckj8jjNcj+M3Je2OyH8yD+kVeXtoAuORtB8sxBbsegHv7VzPjLO7bm6nOefpx36VY5bp1xtTu5YliZJyT3NYBRtOFDrvErIkZyOvBmtX1gx2rYRihstSmsmhIouRU7KF3CyASYk8D3jpWJ1Pp+uP0mnfCrG5mMSFE/mB9faq1SLLwT8Nm47728tttpCHLGAcNGFgj/iu08N8GRAYydpVZ5VDEjGOgnGYnma5fwbxMae66N8hZd3odign8uK7O1eldysCTtPEeSTAj2nNUq014Nqd9m2VjiHEdVlG9vMKdtklZYQfNA9ATH5R965v8PSFuKrbSt29E8fPuAI6/NxXSLbbaQTkg5jAnoPQU7DX8QvcfeayuRvXnDERx2FbrPsnJrec7nDTlR5snGcHpxRraq878MOo9+w/X0pC1c2gyAT/u/wCOtMJqiYJmF9MAnkCOlVjW3R2dFaFqQdzYBWc5n7GKP4fZ2bgYYMfLJ7TVL8UPacjyuNuZE8x045q8vX2dLNySp2ncAAAZgT7g1xylhFvFH3DaDxHoen3qbIBkQOAACMe1Dtum1jAkQoA/mPSPWf1rLV4vjkNn3PIAPeP0rOuBRLrsPmySv1FNaPUOsEYyCIMGRVdqL8QrA8wxnp0kdPpTVh1AIIBgZPJ6nA/vVNsVfP44pF1SpPxHdxJ4UshRSJgwqEcYkR1lhPHhLlVZiQwBuRENcDhPKcoqggCcycAYrm7N9WHt398ULU3GFuBks0+Xp2j04rUyqiy13j9pFCMG5tblEQ6oqI6tnHykj1akdb+IfjCWRrZd0YfKULBNjgcHaRtae89K4XX3XV2DtLTmetG0l8ypdpmQsfy+tdbP0tPU/DPF7W2ygQhrbEziZIYP+eyP+ilW8QE3hsZi8fDaRIb4fwrjNjlh5jHUD3qh8NulYB5x1GPpRblxwH2ZYZIXnPftXn9r0HRN+K7Vt1JtO4CIhBjEEHBYkDInEccUjY8bD7FO9gqXUMEH5wQpQHBIB6xXKa26xIkiDEgHI+swa1prrFlVZho5EZ6d61LaHdXfHFUIHtkTc+LtmcfFd9kTE7WUSBOI44rr2p+JcZhOY+aZwAOrMenc1Vay27lRhioHHSPWj29QVhCktGI6gyc1uZN6WdgzPpVWmqUPcvGQN4QQJlUkExGJYt9hUdVqkFm4QT8jCRHzEQAPqYrNNZ+BZS1G6Fz6lsyPqTRctr4l8UXTO/yKeB8xpV2Z32Wm2gn+YYAHO6kNZMbU3CQWZZAMDnNLaZisHcfljiB/yeM1mTfYxm7yl4t4jbJ2MreTClYhu5IPcyeetc9ddT8oI9KJrj5iTyenYetRs6R3G5VJGew4iY7812mpGr3oANGaKbbNkAtPbJp3ReGSGZwZWRt6SIwY9+Ke07BWIKhQFmQcjAI4Ofam5T4PVW6nw7baDiQZggxkdx6cfekEWroatXO1lEdOgye81VXbZR2U8gkfYxOapb9VnLIxFWPhLMm91OUAI8u7kx+XNIKavPw+Vi5OSVEc8yee/IMVW6RUXA9wM8+Yec8LunqY5IPvXW/hjVC5Ze1/QYUnmJxVM2lQuwELu5UZVm5XHSD/AHoGn1PwdQ6IwUQRPqY5k5yaPbVF4W/ht3a94SRF9yS3qExzziup190hFdWAJmB3kce/Ga47wq5/rXVgO7NgdDKJu64ror+rKIBAiJGY8rfMvv0+1Wwpd7NmRn1mspS7q0BI38YrKzqrVczpLe9guBMtLHEAE/2/OnfDtUEkFVIBOG6z1B6RU/DdMoveYY2AyMFTtyYj8o60nZ0xa4EyNxxP8wnB3dB1mtb5ajpX1dt0ACqCQRC9YEzJ9YpezdT4aWiWVhuILHKsPlHsCarNbqGZ3JgELACAAYgYiMQAZ5zQN7EJcM7txAgTHGYPt1rOtq10OluMIUkboGTA+8dsU54aokCFDYZYHfoftHvSGndB53LFm8m4YiTExxzFYhWCJZSBE+qncAY4x16Vz+hPXK5eQu3id8cboMUvcusm0PyzE4ySoEAc4Was9frQyq+dp+br68emar9PelzcABAELgQI+U+o5M07SOjvy+xlIB68NH+KNqtS0LsyVYggdV4EDvFbt23dsowaZnaVxzuHT1oGn0L/ABCwJ5mf+KuFrhXHw4O7M7bVn2PGIHvRk8Pbds2lEXqwHHcc4roLFkn5gpjr1rLjq7AEzmPSi50yAaawqoIfjlesdz6VDT3oYs3mzyHOBEAwAZpq8ltFTzQwkNxkEypEiJn+9VF9GLFyfIwIhQJlTGQvvyaJGeU8TAVlZgQC3lVcjiF5o1uxcQqoZIuDcrEEQR1BielRs303sAUL7YMJjgBhLcEDqKZvaNggAiP5VERnv2mtdGIab4ruUABCZJnmOGzyKYvXwoZ2Lh1VVBCysHndPHFLayzdVnO6CB5yIZYwse+aTdywHO1sbiOSPU89MUGI6m/8iTALgkDsIMn6xVrpnZwzH2A5mOD6VTJpWubiJGwBfUE+Zp9OB9KasM4GMwAeQInk5MzVejTR06qWgncysJHIPUr6yI+ta1YAtJkblJnHJ6SO9MttbYwnBnyn+qRmfrVB4rqMAA4kn+39qcd049qjWtLGr/TX0CqgIXyjdOMQvDEcccdq5u4Zq01dsoyR5GEEycjoOeOJ+tbynUG+RdNqSls7w0M7EuBnGYHeY/Kr10TYfhqHIBIEKWJUADPOc1zGnunZEFsiBE5YsDz1ziuu8J0pRAx8pOfYHpnJ/Ss2Kqux4eV2O6M4HCIBM/1O37+lVX4hsgXeQJURPPJ5q+a2yOCYK+g3c8RMHik/H/DfiFHSZCGAQMgfy+hya1MuRXNARg/8U94WSLqZgGd0doJz+VLWEBwJB/L6g1beFaYBtxJwD068Dma1afXja2QlW8o7iInr5j3nn8qT19tg4fYsxAn5vseTTtpd84IE88iQRMY5oWr+YsABtBKbp5PP2j0zNYlEI+BOfiOVkHfuG7B4GT7x0p7X6xUJVgSTOOY56/Y1QG/594YhuTHfqfQ1B9aWYyYHTiela0eB7t8Ek7Dn3H5dKys/iv8Ad+dZStGrOqCXFiAGCjiSJzI9Ok0vqncXbbqDwAYmD5j+oOelTdAhQrJ2kBTEemevAio3rmVaTAEAc8sZ4/eKzrVZa07s7szzuIPI4EgAKeAADxFN3CbSlVG4RJjMyR3Ezmt6ZDLE7SoKtggYB5YH7fbpRE1Kq6Ek7ZYRgfzEcwYx+RrO+Sje+Tcdu8jyjsQQ2fXMZH2rbHeA8ss5dY9BPPTjHrSfirzcKoSQRPHpMCeMR1oui1Y2DkMJ2kR0HYz9qdcbC10QhBKyCNsHykjzc9yR0prw/T7N+1QstEDpnGeMAfnSw1UoAx6EGeY5GT8o5PGKg9wm2ssBJ3EkxIg5MDnisqQ/a1QUkc+YEgnmfmmpajUgSQIEzA7djSWjAubGUyZ80HJEyBPemNNpXk7pOSRuBBzyM81mxpG5qQBJH2/WKlprifODEzAIBiOvpQdTpCpJkzxgwI6yaUQvu8oEERGJjuP6TRINg6+6WY+V2aYDA4M9sZz7VC672z/qCSADuYyApHywBjpTR07qoCIygZlvKxnlT6VWa3TFFZ3ktwAwMEkwSCefeuk54GtiMEugQkOSQNrqGgQcghRMTmaBvXYQDdXiCWgN5jMrx2iD/ikE1bbuuPtPcAU7pdXM/EAZZmeDPt0FbuNikH0l6WUktKkEz1AJ3cGrDU6u27yFACehzgnJYf2qrfXqRCqSm7hcOZ7tEmpXQSisQc4IncYE7iyzuUbR1HWsXHZnfJnTa8KiAMxZp3LwAWMzIycY+lO2yzbkB+aD0BAUR75zilvDEBuAzjbAzO0D2gQe1O3HRWY3EmGGUIk5GAf7DNZt1V7J6MJsTYwecEeqzHPqa5vxu9uckegjtAirqz4iiElkVFBJKA+dYmIXv7kVz3it0NcZhw2R7GtYS7UpOyJYTxOfan7r/MGHHmTIiSJbA5P1xFA0iQjOYIPliYOcj3Ein9Rs2MVAkuACTLwYOc4wf3zXS80zsX+DKKXLTtAwI2A42k5+YcxTXhXiuArk7mIgmTunOf6Z/vW7CKyuGuB9wkqmFUDJPvisWz5A9uMyWHzcKOv3IisZDKp67xHadqsrH0IkHtnjtFGZt7KGVxCksOs+WYB9zzVO2nLOXxswxgHr0MZ+2KsLWoloJjcvPMQ0wJAj7dKL0ztWeK2glzeogMcjsSJ/fsatvDrqspBkE8EUjqAHV1JJMeXrkRHt2oHht6BT3HXx34uCwAI3eYYO35uD5j3wJqp8V1Lq20nygRtIM+sz9/rTl66u8OQMwJ7Zzz6TQvGbBeHyw8stGIPrxPHHenHtnKaqlNxCZUQOoJn86c1mhUWhcZ4dvlXmVEZJ+tL6jRhR5Wlv6QDjMZJx/wCakbp+EZ2hvkIIztAPH3/IV0BGsre0fsGtUbjLqdTpXLKVAdVj5JHOchQARmgvKeYJt6EfUyPfMz6e1E0viJWEkBVg5JJ6nERwAf04odzVfElfiIg3MwB6HykDJ6yftQ6aRCid0KRJnBnMduYbpU0QNIHcROCzT8u7/FKKm2QxEgKwB44B53eoyanpnZPNvCkkMPPnE9sGs2RWCanT7mXylWABOcyAMRzxxx9anpdDvyoddsQIIAk4xEnHWZoen3s24OkuT6y+SfmBI5wQelWFnUMAxZiwE5wSIODmfv6e004EnDNSNo8rZ6yIBHXH74rVm8CiSxEAg44JxPvFLvr/AD+YsecEDGCI/Pn2qd28BtRSTIXb9eQZ7En9ms0fTnhzbI24BGQR1Ex9+cdqs9BeBxvUwSDBg/X99apNPcBG0kQuCwHBJkA/59a1q9Hs3OjkMSD0IgEx+/SrjfJi11uptrIuokzKSSxM9p6/SlbOtH8qImBuIKQY/qEZFVly2XIZzvZsbjAI7FYNTTw/axG1gpByZAMdGgzH0rVkK0v+JBRBI4kt/KBxAxz2qm8QtveVDbViZ4AO4AwRgTMGfX0pq7aYqEhcESeRPMqSOc/nRNdq2SzBJ3TBMxmJaiTTFqoveEsiksyqwwZbPtHIPuKHb0bsILKY4ifrOK3aHqO8T+tauuGIEkQMkCev9zXUrPSqtuNu3rJbdB/9J474rWu0auxeQF2hUJgZY54+w9jStkbpAmB/UCZ7SQIq2S+pwy48sz5fLnPeMH71zy1EH4bZVEaXBuFQIBkRI5jmDFB8f+WAwUzMDmYEz1/8d6JYA3OyncCYI28DdIlpExJIHYVWeJa91bupAkEH6z2z/aiTeTP3lWO4U4z1j+Xj86XduOg6ZmjPeBM9OoGJ/wAUNvlkccccda6miKhKT6/lFDLkQMj3/X7RVpY0RKgZn3EEmAMdDmI9aQ+AWwoBI52zE9poliOWNKAPI6sSCCuQTg4k/vFM6XVMEVWXqfQyBMHOBjjnNVKOYHAzg8Ex/jvTzXGkA8YHPJOSf8ms5QU8mqZbcqDunDCYG0geaVM4/WlL2tSfIpHlAMsTJkz7e3pW9ZqiqBRGRJiD9Y6ZPJ9Kr72o3KBCgiZ2iJHc+tUgWfxt4MKoYf0ryDz7kUpaOwkggrPKmRnIHv8A4o+iQCJUg4IafLtI65itazaGVAMGTPXsPyonemsbqtOxLCY7+bA+tN/Hb4YDMWQshVc8BTmB6g/Yd6U17huQQB0GcenvmlWXcNoyFBPQE9T7wB+VM5jWSx1WlRUlZeVwVefMcqpB5OekccTQtPowzAOHA4MiGJjEHiMfakg5JCbZYnkE8ESAB6Vb6O2FGGYkYjiAZB54/Wq8M7I/wxHy25HcFoPeMd6yrB0HZ+B/T2FZWfanlXrZjcW8oBEyZ5jmJ/LtTdmz5Xl5JkjEllAlpLDiIxia1WVqklrtOQxAxhcSevH0rH1QHAyQBnI4jGRFZWUzpB2bnlhveRkiMQM1b6LWqfMWiTGyGghtvBBx1OaysoyUR1SKEMTJLCSf5RkQAP1zIpZ9QfiAuvAAgH0kf5rdZWcRFvpgrEwGXvMMIHAHWfWmAygEAgsSFggxPuO4NZWVn91a+pXrSMoBZlKkQV4B+0+lMTuVp8wgc9cc84NarKFk57UuSXCiMCB2IieZ6Co6xdypuBLngCI6KDJ9APXNZWV1cw20YXaGaGMkjJEfb0oum+HhQ0uxwdp2jELz6+lbrKreGxhcIDKxRdpIgr5WaedyLuEEds01o7zsd92AYWOxAgDCk9Pat1lYvTNSaAGKcNMxgKTPTE8zMdYqk8RuAsAQNvqJ69O1brKsWPpK+F2+W2AcyZJ4PSSY/eahokJI247n7zjvWVldG1hZubQFbOc449+4/wAUIA5jA4jH9q3WVTsgWAS0mYBiREyZjn2NaFwh5PB79YPWPUVlZVexekLl1pYcA8jHHQTUdNaDNtP7MYrdZTOgYsXQQVMhlOO3qAf80VjBgjIkflyPrFZWVipvbtLAt5dp75EQePal7tpRGwHjdzB28ke9ZWUwztY2LAKSCZMxx/UBn1GeIqKu6KCTAJPWc9Zx2j0xWVlZMCuagTienQdqysrKC//Z'),
            fit: BoxFit.fill,
          ),
          // shape: BoxShape.circle,
        ),
        height: 100.0,
        width: 100.0,
        // color: Colors.red,
      ),
    );
  }

  Widget _buildWideContainers() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFRUYGBgaHBocGhwYGhwaGBwaHBocGhgcHBocIS4lHB4rHxwcJjgmKy8xNjU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHjQrISs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAADBAIFAAEGB//EADwQAAIBAwMCBAQEBQMDBAMAAAECEQADIQQSMUFRBSJhcRMygZEGobHwFEJSwdEjYuFykvGCorKzBzND/8QAGQEBAQEBAQEAAAAAAAAAAAAAAQACAwQF/8QAIxEBAQACAgMAAAcAAAAAAAAAAAECESExAxJBBBQiQmGR4f/aAAwDAQACEQMRAD8A6hqgagzVFrlaTVyq26YNPF6XuqDUiz3QBSFy8ScUzdbpQkt1IXToaaVajaWjAVJtVoiitA1B7gFSFxWjSyXDNMA4qQLkmtotS21ICoNTS2ptMRim9lB1N7aMCgqpdOV5rGf0rWo1cc1EOsTQgtL5nwKs71rcIIqr017a8gU3/GsTxipD2NIRk0YiOBS66tsYqwsWWbkVoFPhk0e3pDVimmAoxtxUVZqSltC7sFURJPriuX1/4pUMRZVSo/neTPsnbPM/Slv/AMg65zdFokbFAICwTJ53Z+wjtXI/FmBmB+4NFqei+F/i603lujY3cZX/AIroyVZSymRnI4xzFeLbo4/XpV74P+IXs+QeZSQCCSAJ5A7Zk1bOnoF7WAIRVGNPv4NNko9rep7/AEIMEfeqfTbwfKTQFnd0u2IOaatIwE0HT2W3jecEVZXLqgQKUXtEzmrfTPCzVdtyPWrA2vKBSIj8c96ys/hqyoh3LtRFyg3XoIeakPcu0NnrGShgVAIpmaMi0REoypQUESigVIJQy1KbY4pVlJNGa5NRVqgyKkjZqNbg1IcisFBF7oaZQYoKF14FJJd3TNO32ilGs9ZoRA6ZGeWNC1yKvy1jgbjmgXEPWpA6RvNTj34xFIadfMcxVpp0XkmpH9EgkAir+2lUC3FlYNXSalYGaUPFBuvmKMHpTWXVUgmlOQ/HemHwyYG74qsp/qVrexgD6Mg+69689NyDjp9f/Nev+P8AhS6m2Qp2uAdpBMHg7WjlZAPoQDXmmn8BdnZLjbGUmVIlzHJEGCPYmjSU5c/vpXVeD/hZHUNcvbWIBKKFO0HjcZP6UDTaWzYeX23COVccZwy9D3+kdcks3UVxeDqDuJyWVyPQqNpHvjvXPO2cPT4MMcua7nQeDpbti2vmQDqcmczI/tRR4WijyCPczVFovGBgFXWeIGPqQdo7zIq1GoVus/YGuXvY9X5fG9a/oLU2m6nArdiyOa38YLJwy9dy/wB6S0+s52gxJia7YZzLh5PP4L4+fiyW95qaW8WNV1pxM1aW2GIro85lXrKjvFZUldqEHFDtWac1VsTJrdhPSpMKCKWeBTV87aWLAmKENYGKLFQBAGK0L1SbZ6E+a25raLSgyKxVijm3WisVIFea21yoPNRZqgmqTzTSCBULYxRG4oJTU3opDUarHNG1iE1X39MYrKKNczNaOs6UC6pHNKC5BpQy3Duq00V9WG1uaolfNMacZmjaXZ07Dg4psakIoL7o7hXaPfaDH1pTQX2cxGBVR4p+IHt3GVYZi21JjyiY/lOcgwOardNYyXt3Gk1QdAyNIIkHuKzVKGia47wXxV0uN8fUB9wURgBGzzgDtgd66fUakLE9a0KB474v/DWwVgu7BEU8SeWI6gc+uBXBeIaxmcsSd24tu6zM7p/eKtvGrq3tSGD7hbUhk2/IQSAd0+YkwfoRVFqlGQD7e37mlkHW6zfBIz1+vP5wadtWA9pXUDAh16E5yex9f2tExzV14BrEQsCGJ3EwMysAER/NxMc9elc/Juzb0/hrjMtZfUra24gGG6KzG3n0bhh6GPem9OxG4izs/qdnCADspYYE9BNGv3bDoVRW3Pt2h4FtDMHdPmGOuBxntV2/ByylgPlZlgeXIMHmYrlrjdez2sy9cVlfZ74KW28oGVRixP8AudoAOeBIqw/D2ldA6OGIkbZBHTzRPTjjvQb1kWSiZMgQQWKeUBCGRCM9Zg0x4Zqm+KoIiZgA7lPMZ7yJqxy1loeXxzLx3K9rdbRBzTK7oxW77TyIqQws16XzEPiHtWVLeKypLK/bmopeA5opqq1gMx0NCG115SIBzSiCorpYM0wLdSbL4qHWpBBU1WlNotHRKioqe+KkMLZoOogCsuakxSj3qk070vurHehpJMAEk4AHJJ4FCH+OaIbxirnxX8PG1YVhlwN1wc4Pb/p/PJrn5gUJG7eIqvu6g0yzTzSd0yakr9beJ6UopEZqxvL1oGwEZo0idsS0VZWdNSaWZbFWlm2wPemRHfD9SEwRXN+Nad21bvaVVGzcxBgRBknsxgjHP1NdLdQYMVW6HTi4Lzzi4xQH/ao2j891G2pONuNcPZco0bkYgwZE9SD1/wCacteOXF4Y/r+vFdY2gR9wdQwZt0HodoWQRkGBVP4n+EwPNZeB/S/9mH9x9aWVQ/iLEkgAFiS0YDE9xxPOfWk7931pcuRUlQtGMnApSAMmi2Z3yDBGZ4/PpQgIOeRU7cQc8kD9ZqpnbpXv7g6sq3GVF8p8pAZQS6Rywn98F7TqvwgMruHzwVkkRtZf6vf79KpPENUEuPtUbwxBeAzYG2FBwoj3pnQK72SNwEGSGbzkgyCD3AP6Vwyx4fR8fklzs7p9rzhFZ2UDcwwCwHmYeZpwDsJ+1aZAbik/K3zQI6rPAg4PJmlk1bQ1p/K27duA5k5n1nb07jqaIXIUTAPG4QBEHGBjj95rGtV18duWNmTttVaYALgggQR0PYA9Ig84z0il9jDBotrUF0tuOCoP+amz969T5FD+HWUXfWqUsC1Ae3NFE1MLQSeBWiRRNRFKo9ISC5phVoVumFFSbUVp1qZobNUi9/iknerFqRuWqEV311/4J8K3H+IYeVSQk9W6t7Dj3ntXO+FeEtfurbUwDlj/AEqPmPv0HqRXqELZRUQABQAo7AVJDXiT3gZ+vE151434cbNyAPI+U9O6/T9CK7+1fVTLMAGwSxAEngSepNL+N+FC7bZehyp/pbofb+xoTzZyAKTKg0fVKVZkbDKYI9RQbdSZ/ChqT1emjAq0RJqT6URQnOpuDVcaS6RzSt9RvFFc59KUPrru227/ANIJ+vT8614ZZZLSq44UT78n86HqCH+Faj53Wf8ApTztPoYA+tXHiAxAonNavEkI23XJqF24AjmeFY+0A0fT2BxFC8VsbbbmP5H/APia0y80VATH68x3PatXWkz06DpFHaFScS5MD0Bifv8ApULOlLZJCju2PsOakHdJJ3HJOSfU80fw23udByC6D/3Ca09nEbg2QMdDmP0NNeFn/UT0J/IGi9NYTeUn8ta9gbtzj5259yKa8GPzBfmHmTkieCInjiqp7zSTnJJ69TNGsOwMyQeRntmP32ouO8dN4eT0z9l9qnVwrukpADR8yOB074g/XvNDv3QFUK8gEGSOkz9+lVg1xk7vlf5o69jHQip2WHQg+5rjcbO3ux8+OU4/16J4Us2Lcf0A/U5P50bZmkvw1qg1hOwlftzVuxFeidPnZzWVA21qiRWUsnUcVLdSFnUAyZxwO1MG8qiSaydF9UDSioRT73AVkGq3U6qAAIlsCnoGbBM0/bFVdjVQm4wCDBnFWaGc1SnSbigOKZIoDikF3agM81YP4VeP/wDG5/2N/ilrvhd5QWNpwoEklGAAHJJjiip234W8PW1Z3Aqzv5mKkEY4UEdv1JoPi3iiWlLuZJ4UfMx7AdvWuN0dvU2XUoLiF/l8phuvykQ3ToaLr/D9S5Lvauux/wBjfYADAoSt1vib6hy1wwo+RB8qj+59f0GKsPB/xZc0/lb/AFLXG1j5lH+1j+hx7Us2he2k3LLoOJZCBJ4yRQn8DvnI092Onkb/ABUll+K9Nb1Fv+L0zbigAuLw4XoWXpHfqOuK5TTuTV0vhWpQF1tXbZUGW2MPLHmmRBWOQcULTeB6gru+BcIIkQjQQciIHFFQSsamSxpm34PqRzp7v/Y3+KNpivWpKU2vOJqw1OlETWtUi7wRTrMpEnina0p/Dbe7Us0YtoF/9TncfqAo/wC6rh/M0GlPw3b/ANNnIzcdnPsTtT/2qKcvqAZmjHpq9pfDC8VXeMXh8NyxAUI0+xUiianVTAzmuZ/Gep2IiAxvJJHcLH5SR9q0HOaW2XYNtVzIVUZu0ASJ+XzD7HpNOqwBIdyIOIQhfUoVYMBP0MTVVZuBGV58x3SuZXECffn6UxqfFS4AKAkcNwRXPKW16PFlhjjz2Z8SvhlMkuRthsSVg4MjkE8jkRVXbusDuUwf3xQnuE1H61qTU0455by3DWmwZOffNN29KzsBbX5jAk4BHJB7QJPtSKX9oxzwJ6evvW3uOpB3mSo4PQ5j9KWDNx0yCxDAkfLiRzMGgM4iBsPrBB+9Lg1Y+HWXUpf2rsDnaXBKF1EjdHCgkZ6H61J6H4Rp1TT2gvGxT2mRJMepJNWdsUn4FdFywhAI2ypBKkhlO1sr5TkdKsQtaTUVlb21lSc1o9WoSZ8kkmeJMxNInxUhypIjceOR2APtVdeugIUmCWEj64+hpNggmH3MDgetcZa6WOnfxkgfLPel7Xig2+fHJE95xXPpqGJ7RM+1WCaPchYmcTA6Ue1nYdI6b03cBhP1jmm/CtcmxVLiVxz2rh/4q5hdzBRjrTOg00uqtO1uo59xNa9tLT0a3cBEgzR9GP8AVtn/AHp/8hXFeHF7M+ckFoIORzz6V0dnVFhxxTM5WLXbeLq2+VXVEbR/+l0VeT0Ygz/xVP4nvFq4SmtA2tJe4hQCM7gGkr3iuWGsd2w7df5jx96FbuuQ+53KnEFiR6jmqZRPQ7jRd0Hqtz/6RVVrrGrOpcxqTZ3GBauBTEY2ywAE1x29iwliVGVJY49u1bOrct87kHjzNH61XKF2Piuk22Euu+pB+LbGy/cDAf6gEkDHEnmrTxxX3jaurYbebDoqzJ5DEHd69orzXUIzcuzDpLEgfetC+8AK7/8Ae0frWblA7HxBLnwn8mvHkbL3UKfKfnAaSvf0repZNPY07ve1Z3qnlS4AAdgaIMQK4mxfcOwZ3MgcuxEA9pol/eWCsWPUAkkD2HSr250noX4b1lu5ve1d1DuoI+FeuA8wQQOMkRM4zXFGd77lKsWYlTyCSZB9qR0zFZKkj1BjHaaYtXJMzJ6yZ/OtS7MavWzNQ8TcrZIEbnhFkxJc7f7mmWf0pW4xe9bTMJucieYG1ZHu0j2NVM7XOnthEVV4UAD2AgUvrCenWiFsc1T66+S6RkCSc44oyy0E9dfTYqsckj8jjNcj+M3Je2OyH8yD+kVeXtoAuORtB8sxBbsegHv7VzPjLO7bm6nOefpx36VY5bp1xtTu5YliZJyT3NYBRtOFDrvErIkZyOvBmtX1gx2rYRihstSmsmhIouRU7KF3CyASYk8D3jpWJ1Pp+uP0mnfCrG5mMSFE/mB9faq1SLLwT8Nm47728tttpCHLGAcNGFgj/iu08N8GRAYydpVZ5VDEjGOgnGYnma5fwbxMae66N8hZd3odign8uK7O1eldysCTtPEeSTAj2nNUq014Nqd9m2VjiHEdVlG9vMKdtklZYQfNA9ATH5R965v8PSFuKrbSt29E8fPuAI6/NxXSLbbaQTkg5jAnoPQU7DX8QvcfeayuRvXnDERx2FbrPsnJrec7nDTlR5snGcHpxRraq878MOo9+w/X0pC1c2gyAT/u/wCOtMJqiYJmF9MAnkCOlVjW3R2dFaFqQdzYBWc5n7GKP4fZ2bgYYMfLJ7TVL8UPacjyuNuZE8x045q8vX2dLNySp2ncAAAZgT7g1xylhFvFH3DaDxHoen3qbIBkQOAACMe1Dtum1jAkQoA/mPSPWf1rLV4vjkNn3PIAPeP0rOuBRLrsPmySv1FNaPUOsEYyCIMGRVdqL8QrA8wxnp0kdPpTVh1AIIBgZPJ6nA/vVNsVfP44pF1SpPxHdxJ4UshRSJgwqEcYkR1lhPHhLlVZiQwBuRENcDhPKcoqggCcycAYrm7N9WHt398ULU3GFuBks0+Xp2j04rUyqiy13j9pFCMG5tblEQ6oqI6tnHykj1akdb+IfjCWRrZd0YfKULBNjgcHaRtae89K4XX3XV2DtLTmetG0l8ypdpmQsfy+tdbP0tPU/DPF7W2ygQhrbEziZIYP+eyP+ilW8QE3hsZi8fDaRIb4fwrjNjlh5jHUD3qh8NulYB5x1GPpRblxwH2ZYZIXnPftXn9r0HRN+K7Vt1JtO4CIhBjEEHBYkDInEccUjY8bD7FO9gqXUMEH5wQpQHBIB6xXKa26xIkiDEgHI+swa1prrFlVZho5EZ6d61LaHdXfHFUIHtkTc+LtmcfFd9kTE7WUSBOI44rr2p+JcZhOY+aZwAOrMenc1Vay27lRhioHHSPWj29QVhCktGI6gyc1uZN6WdgzPpVWmqUPcvGQN4QQJlUkExGJYt9hUdVqkFm4QT8jCRHzEQAPqYrNNZ+BZS1G6Fz6lsyPqTRctr4l8UXTO/yKeB8xpV2Z32Wm2gn+YYAHO6kNZMbU3CQWZZAMDnNLaZisHcfljiB/yeM1mTfYxm7yl4t4jbJ2MreTClYhu5IPcyeetc9ddT8oI9KJrj5iTyenYetRs6R3G5VJGew4iY7812mpGr3oANGaKbbNkAtPbJp3ReGSGZwZWRt6SIwY9+Ke07BWIKhQFmQcjAI4Ofam5T4PVW6nw7baDiQZggxkdx6cfekEWroatXO1lEdOgye81VXbZR2U8gkfYxOapb9VnLIxFWPhLMm91OUAI8u7kx+XNIKavPw+Vi5OSVEc8yee/IMVW6RUXA9wM8+Yec8LunqY5IPvXW/hjVC5Ze1/QYUnmJxVM2lQuwELu5UZVm5XHSD/AHoGn1PwdQ6IwUQRPqY5k5yaPbVF4W/ht3a94SRF9yS3qExzziup190hFdWAJmB3kce/Ga47wq5/rXVgO7NgdDKJu64ror+rKIBAiJGY8rfMvv0+1Wwpd7NmRn1mspS7q0BI38YrKzqrVczpLe9guBMtLHEAE/2/OnfDtUEkFVIBOG6z1B6RU/DdMoveYY2AyMFTtyYj8o60nZ0xa4EyNxxP8wnB3dB1mtb5ajpX1dt0ACqCQRC9YEzJ9YpezdT4aWiWVhuILHKsPlHsCarNbqGZ3JgELACAAYgYiMQAZ5zQN7EJcM7txAgTHGYPt1rOtq10OluMIUkboGTA+8dsU54aokCFDYZYHfoftHvSGndB53LFm8m4YiTExxzFYhWCJZSBE+qncAY4x16Vz+hPXK5eQu3id8cboMUvcusm0PyzE4ySoEAc4Was9frQyq+dp+br68emar9PelzcABAELgQI+U+o5M07SOjvy+xlIB68NH+KNqtS0LsyVYggdV4EDvFbt23dsowaZnaVxzuHT1oGn0L/ABCwJ5mf+KuFrhXHw4O7M7bVn2PGIHvRk8Pbds2lEXqwHHcc4roLFkn5gpjr1rLjq7AEzmPSi50yAaawqoIfjlesdz6VDT3oYs3mzyHOBEAwAZpq8ltFTzQwkNxkEypEiJn+9VF9GLFyfIwIhQJlTGQvvyaJGeU8TAVlZgQC3lVcjiF5o1uxcQqoZIuDcrEEQR1BielRs303sAUL7YMJjgBhLcEDqKZvaNggAiP5VERnv2mtdGIab4ruUABCZJnmOGzyKYvXwoZ2Lh1VVBCysHndPHFLayzdVnO6CB5yIZYwse+aTdywHO1sbiOSPU89MUGI6m/8iTALgkDsIMn6xVrpnZwzH2A5mOD6VTJpWubiJGwBfUE+Zp9OB9KasM4GMwAeQInk5MzVejTR06qWgncysJHIPUr6yI+ta1YAtJkblJnHJ6SO9MttbYwnBnyn+qRmfrVB4rqMAA4kn+39qcd049qjWtLGr/TX0CqgIXyjdOMQvDEcccdq5u4Zq01dsoyR5GEEycjoOeOJ+tbynUG+RdNqSls7w0M7EuBnGYHeY/Kr10TYfhqHIBIEKWJUADPOc1zGnunZEFsiBE5YsDz1ziuu8J0pRAx8pOfYHpnJ/Ss2Kqux4eV2O6M4HCIBM/1O37+lVX4hsgXeQJURPPJ5q+a2yOCYK+g3c8RMHik/H/DfiFHSZCGAQMgfy+hya1MuRXNARg/8U94WSLqZgGd0doJz+VLWEBwJB/L6g1beFaYBtxJwD068Dma1afXja2QlW8o7iInr5j3nn8qT19tg4fYsxAn5vseTTtpd84IE88iQRMY5oWr+YsABtBKbp5PP2j0zNYlEI+BOfiOVkHfuG7B4GT7x0p7X6xUJVgSTOOY56/Y1QG/594YhuTHfqfQ1B9aWYyYHTiela0eB7t8Ek7Dn3H5dKys/iv8Ad+dZStGrOqCXFiAGCjiSJzI9Ok0vqncXbbqDwAYmD5j+oOelTdAhQrJ2kBTEemevAio3rmVaTAEAc8sZ4/eKzrVZa07s7szzuIPI4EgAKeAADxFN3CbSlVG4RJjMyR3Ezmt6ZDLE7SoKtggYB5YH7fbpRE1Kq6Ek7ZYRgfzEcwYx+RrO+Sje+Tcdu8jyjsQQ2fXMZH2rbHeA8ss5dY9BPPTjHrSfirzcKoSQRPHpMCeMR1oui1Y2DkMJ2kR0HYz9qdcbC10QhBKyCNsHykjzc9yR0prw/T7N+1QstEDpnGeMAfnSw1UoAx6EGeY5GT8o5PGKg9wm2ssBJ3EkxIg5MDnisqQ/a1QUkc+YEgnmfmmpajUgSQIEzA7djSWjAubGUyZ80HJEyBPemNNpXk7pOSRuBBzyM81mxpG5qQBJH2/WKlprifODEzAIBiOvpQdTpCpJkzxgwI6yaUQvu8oEERGJjuP6TRINg6+6WY+V2aYDA4M9sZz7VC672z/qCSADuYyApHywBjpTR07qoCIygZlvKxnlT6VWa3TFFZ3ktwAwMEkwSCefeuk54GtiMEugQkOSQNrqGgQcghRMTmaBvXYQDdXiCWgN5jMrx2iD/ikE1bbuuPtPcAU7pdXM/EAZZmeDPt0FbuNikH0l6WUktKkEz1AJ3cGrDU6u27yFACehzgnJYf2qrfXqRCqSm7hcOZ7tEmpXQSisQc4IncYE7iyzuUbR1HWsXHZnfJnTa8KiAMxZp3LwAWMzIycY+lO2yzbkB+aD0BAUR75zilvDEBuAzjbAzO0D2gQe1O3HRWY3EmGGUIk5GAf7DNZt1V7J6MJsTYwecEeqzHPqa5vxu9uckegjtAirqz4iiElkVFBJKA+dYmIXv7kVz3it0NcZhw2R7GtYS7UpOyJYTxOfan7r/MGHHmTIiSJbA5P1xFA0iQjOYIPliYOcj3Ein9Rs2MVAkuACTLwYOc4wf3zXS80zsX+DKKXLTtAwI2A42k5+YcxTXhXiuArk7mIgmTunOf6Z/vW7CKyuGuB9wkqmFUDJPvisWz5A9uMyWHzcKOv3IisZDKp67xHadqsrH0IkHtnjtFGZt7KGVxCksOs+WYB9zzVO2nLOXxswxgHr0MZ+2KsLWoloJjcvPMQ0wJAj7dKL0ztWeK2glzeogMcjsSJ/fsatvDrqspBkE8EUjqAHV1JJMeXrkRHt2oHht6BT3HXx34uCwAI3eYYO35uD5j3wJqp8V1Lq20nygRtIM+sz9/rTl66u8OQMwJ7Zzz6TQvGbBeHyw8stGIPrxPHHenHtnKaqlNxCZUQOoJn86c1mhUWhcZ4dvlXmVEZJ+tL6jRhR5Wlv6QDjMZJx/wCakbp+EZ2hvkIIztAPH3/IV0BGsre0fsGtUbjLqdTpXLKVAdVj5JHOchQARmgvKeYJt6EfUyPfMz6e1E0viJWEkBVg5JJ6nERwAf04odzVfElfiIg3MwB6HykDJ6yftQ6aRCid0KRJnBnMduYbpU0QNIHcROCzT8u7/FKKm2QxEgKwB44B53eoyanpnZPNvCkkMPPnE9sGs2RWCanT7mXylWABOcyAMRzxxx9anpdDvyoddsQIIAk4xEnHWZoen3s24OkuT6y+SfmBI5wQelWFnUMAxZiwE5wSIODmfv6e004EnDNSNo8rZ6yIBHXH74rVm8CiSxEAg44JxPvFLvr/AD+YsecEDGCI/Pn2qd28BtRSTIXb9eQZ7En9ms0fTnhzbI24BGQR1Ex9+cdqs9BeBxvUwSDBg/X99apNPcBG0kQuCwHBJkA/59a1q9Hs3OjkMSD0IgEx+/SrjfJi11uptrIuokzKSSxM9p6/SlbOtH8qImBuIKQY/qEZFVly2XIZzvZsbjAI7FYNTTw/axG1gpByZAMdGgzH0rVkK0v+JBRBI4kt/KBxAxz2qm8QtveVDbViZ4AO4AwRgTMGfX0pq7aYqEhcESeRPMqSOc/nRNdq2SzBJ3TBMxmJaiTTFqoveEsiksyqwwZbPtHIPuKHb0bsILKY4ifrOK3aHqO8T+tauuGIEkQMkCev9zXUrPSqtuNu3rJbdB/9J474rWu0auxeQF2hUJgZY54+w9jStkbpAmB/UCZ7SQIq2S+pwy48sz5fLnPeMH71zy1EH4bZVEaXBuFQIBkRI5jmDFB8f+WAwUzMDmYEz1/8d6JYA3OyncCYI28DdIlpExJIHYVWeJa91bupAkEH6z2z/aiTeTP3lWO4U4z1j+Xj86XduOg6ZmjPeBM9OoGJ/wAUNvlkccccda6miKhKT6/lFDLkQMj3/X7RVpY0RKgZn3EEmAMdDmI9aQ+AWwoBI52zE9poliOWNKAPI6sSCCuQTg4k/vFM6XVMEVWXqfQyBMHOBjjnNVKOYHAzg8Ex/jvTzXGkA8YHPJOSf8ms5QU8mqZbcqDunDCYG0geaVM4/WlL2tSfIpHlAMsTJkz7e3pW9ZqiqBRGRJiD9Y6ZPJ9Kr72o3KBCgiZ2iJHc+tUgWfxt4MKoYf0ryDz7kUpaOwkggrPKmRnIHv8A4o+iQCJUg4IafLtI65itazaGVAMGTPXsPyonemsbqtOxLCY7+bA+tN/Hb4YDMWQshVc8BTmB6g/Yd6U17huQQB0GcenvmlWXcNoyFBPQE9T7wB+VM5jWSx1WlRUlZeVwVefMcqpB5OekccTQtPowzAOHA4MiGJjEHiMfakg5JCbZYnkE8ESAB6Vb6O2FGGYkYjiAZB54/Wq8M7I/wxHy25HcFoPeMd6yrB0HZ+B/T2FZWfanlXrZjcW8oBEyZ5jmJ/LtTdmz5Xl5JkjEllAlpLDiIxia1WVqklrtOQxAxhcSevH0rH1QHAyQBnI4jGRFZWUzpB2bnlhveRkiMQM1b6LWqfMWiTGyGghtvBBx1OaysoyUR1SKEMTJLCSf5RkQAP1zIpZ9QfiAuvAAgH0kf5rdZWcRFvpgrEwGXvMMIHAHWfWmAygEAgsSFggxPuO4NZWVn91a+pXrSMoBZlKkQV4B+0+lMTuVp8wgc9cc84NarKFk57UuSXCiMCB2IieZ6Co6xdypuBLngCI6KDJ9APXNZWV1cw20YXaGaGMkjJEfb0oum+HhQ0uxwdp2jELz6+lbrKreGxhcIDKxRdpIgr5WaedyLuEEds01o7zsd92AYWOxAgDCk9Pat1lYvTNSaAGKcNMxgKTPTE8zMdYqk8RuAsAQNvqJ69O1brKsWPpK+F2+W2AcyZJ4PSSY/eahokJI247n7zjvWVldG1hZubQFbOc449+4/wAUIA5jA4jH9q3WVTsgWAS0mYBiREyZjn2NaFwh5PB79YPWPUVlZVexekLl1pYcA8jHHQTUdNaDNtP7MYrdZTOgYsXQQVMhlOO3qAf80VjBgjIkflyPrFZWVipvbtLAt5dp75EQePal7tpRGwHjdzB28ke9ZWUwztY2LAKSCZMxx/UBn1GeIqKu6KCTAJPWc9Zx2j0xWVlZMCuagTienQdqysrKC//Z'),
                fit: BoxFit.fill,
              ),
              // shape: BoxShape.circle,
            ),
            height: 100.0,
            width: 100.0,
            // color: Colors.red,
          ),
          Container(
            // height: 100.0,
            width: 100.0,
            // color: Colors.yellow,
            child: Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non libero ut elit sodales iaculis. Proin egestas lacus non arcu semper, at aliquet neque aliquet. Vestibulum vulputate libero nisi, quis vestibulum urna fermentum eu. Nunc sagittis dapibus risus sit amet volutpat. In elementum diam eu lectus molestie, quis aliquam mauris tincidunt. Nulla urna magna, luctus sed aliquam id, dictum id massa. In at pulvinar urna. Nulla gravida ex sapien, hendrerit egestas arcu suscipit tempor. Vestibulum semper facilisis volutpat', maxLines: 15, textAlign: TextAlign.justify, ),
          ),
        ],
      ),
    );
  }
}


